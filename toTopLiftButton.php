<?php
/*
Plugin Name: To Top Lift Button

Plugin URI: https://gitlab.com/naejel/toTopLiftButton

Description: Add a button in the bottom right corner which link the top 
of page

Version: 0.1.0

Author: Jean LE QUELLEC

Author URI: https://lequellec.xyz

License: MIT
*/

add_filter('the_content', 'toTop_add_button', 20) ;

function toTop_add_button($content) {

    return $content . "<a href='#page'><div id='totop' class='small'> <i class='material-icons'>arrow_upward</i></div></a>";

}

function wptuts_scripts_basic()
{
    // Register the script like this for a plugin:
    wp_register_script( 'custom-script', plugins_url( '/js/toTopLiftButton.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script('custom-script');
}

function wptuts_styles_with_the_lot()
{
    // Register the style like this for a plugin:
    wp_register_style( 'custom-style', plugins_url( '/css/toTopLiftButton.css', __FILE__ ), array() );
    wp_register_style( 'google-fonts', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
    wp_enqueue_style('custom-style');
    wp_enqueue_style('google-fonts');
}

add_action( 'wp_enqueue_scripts', 'wptuts_styles_with_the_lot', 915 );

add_action( 'wp_enqueue_scripts', 'wptuts_scripts_basic', 916 );
