$=jQuery.noConflict();
$(window).scroll(function () {
     var sc = $(window).scrollTop()
     if (sc < 200){
       $("#totop").removeClass("big")
       $("#totop").addClass("small")
     }
     if (sc > 201) {
        $("#totop").removeClass("small")
	$("#totop").addClass("big")
    }
});

$(document).ready(function(){
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 1300, 'swing', function () {
	        window.location.hash = target;
	    });
	});
});